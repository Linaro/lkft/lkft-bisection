[bat]
BAT_BISECTION_OLD=62c04453381e4a13674df1b4793788994fd9a696
BAT_BISECTION_NEW=next-20201015
BAT_GIT_DIR=/srv/jenkins/kernel/bisecting


[build]
# Build kernel using Tuxbuild
tuxbuild build --git-repo https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git \
  --target-arch x86 \
  --toolchain gcc-9 \
  --json-out build.json \
  --kconfig defconfig \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/lkft.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/lkft-crypto.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/distro-overrides.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/systemd.config  \
  --kconfig https://raw.githubusercontent.com/Linaro/meta-lkft/sumo/recipes-kernel/linux/files/virtio.config  \
  --kconfig CONFIG_IGB=y \
  --kconfig CONFIG_KASAN=y \
  --kconfig CONFIG_UNWINDER_FRAME_POINTER=y \
  --git-sha ${BAT_KERNEL_SHA}

build_ret=$?

build_json=build.json
KERNEL_NAME=bzImage

GIT_DESCRIBE="$(jq -r .git_describe "${build_json}")"
DOWNLOAD_URL="$(jq -r .download_url "${build_json}")"
# The URL ends with /, so remove the last one
DOWNLOAD_URL="$(echo "${DOWNLOAD_URL}" | cut -d/ -f1-4)"

export BAT_LAVA_JOBNAME="lkft-bisection x86_64 next-20201015 ${GIT_DESCRIBE}"
export BAT_PUB_KERNEL="${DOWNLOAD_URL}/${KERNEL_NAME}"
export BAT_PUB_MODULES="${DOWNLOAD_URL}/modules.tar.xz"
export BAT_PUB_MODULES_COMPRESSION="xz"
export BAT_PUB_ROOTFS="https://storage.lkft.org/rootfs/oe-sumo/20200723/intel-corei7-64/rpb-console-image-lkft-intel-corei7-64-20200723162342-41.rootfs.tar.xz"
export BAT_PUB_ROOTFS_COMPRESSION="xz"


[test]
# Write out LAVA job
rm -f job.yaml
eval "cat << EOF
$(cat <(bat_get_section lavajob))
" > job.yaml 2> /dev/null

LAVAJOB=$(lavacli jobs submit job.yaml)
LAVA_LOG="log-${BAT_KERNEL_SHA_SHORT}"
lavacli jobs show ${LAVAJOB}
lavacli jobs wait ${LAVAJOB}
lavacli jobs logs ${LAVAJOB} > ${LAVA_LOG}


[discriminator]
# Look for specific message in the log:
ret="OLD"
if grep -qa 'segfault at' ${LAVA_LOG}; then ret="NEW"; fi
if grep -qa 'other problem' ${LAVA_LOG}; then ret="NEW"; fi

if [ "${ret} = "NEW" ]; then
  echo " ****************************************************** "
  echo " JOB FAILED!"
  echo " ****************************************************** "
  echo " See ${LAVA_LOG}".
  bat_new
else
  echo " ****************************************************** "
  echo " All fine and dandy."
  echo " ****************************************************** "
  echo " See ${LAVA_LOG}".
  bat_old
fi

[lavajob]
timeouts:
  job:
    minutes: 35
  connection:
    minutes: 2
context:
  test_character_delay: 10
device_type: x86
job_name: ${BAT_LAVA_JOBNAME}
priority: 99
visibility: public
actions:
- deploy:
    namespace: target
    timeout:
      minutes: 10
    to: tftp
    kernel:
      url: ${BAT_PUB_KERNEL}
    modules:
      compression: ${BAT_PUB_MODULES_COMPRESSION}
      url: ${BAT_PUB_MODULES}
    namespace: target
    nfsrootfs:
      compression: ${BAT_PUB_ROOTFS_COMPRESSION}
      url: ${BAT_PUB_ROOTFS}
    os: oe
- boot:
    namespace: target
    auto_login:
      login_prompt: 'login:'
      username: root
      password_prompt: ''
      password: ''
      login_commands:
      - su
    prompts:
    - 'root@intel-corei7-64:'
    - root@(.*):[/~]#
    timeout:
      minutes: 15
    method: ipxe
    commands: nfs
    parameters:
      shutdown-message: 'reboot: Restarting system'
- test:
    namespace: target
    timeout:
      minutes: 15
    definitions:
    - from: inline
      repository:
        metadata:
          format: Lava-Test Test Definition 1.0
          name: ltp-mm-segfault-bisect
          description: LTP mm bisect
        run:
          steps:
          - cd /opt/ltp
          - ./runltp -s mtest05
      name: ltp-mm-segfault-bisect
      path: inline/prep.yaml
